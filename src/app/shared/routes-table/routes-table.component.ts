import { Component, OnInit, Input } from '@angular/core';
import * as moment from 'moment';
import { IonDatetime } from '@ionic/angular';
import { DatabaseService } from 'src/services/database.service';

interface Destination {
  date: IonDatetime;
  destination: string;
  startingKlm: number;
  fuel: {
    fuelCost: number;
    fuelCostPerLitre: number;
    remainingKlm: number;
  }
}

@Component({
  selector: 'app-routes-table',
  templateUrl: './routes-table.component.html',
  styleUrls: ['./routes-table.component.scss'],
})
export class RoutesTableComponent implements OnInit {

  public destinations: Destination[] = [];

  @Input() public routesAmount = 10000;

  constructor(private _databaseService: DatabaseService) { }

  public ngOnInit(): void {
    this._databaseService.get_Destinations(this.routesAmount).subscribe(destinations => {
      if (destinations.length > 0) {
        this.destinations = destinations.map<Destination>(destinationRecord => <Destination>destinationRecord.payload.doc.data());
      }
    });
  }

  public formatDate(date: Date): string {
    return moment(date).format('DD/MM/YYYY');
  }
}
