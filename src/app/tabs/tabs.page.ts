import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  public title: string;

  private _lang: any;
  
  constructor(private _translate: TranslateService) {
    this._lang = 'el';
    this._translate.setDefaultLang(this._lang);
    this._translate.use(this._lang);
  }

}
