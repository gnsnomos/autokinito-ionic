import { Injectable } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { DatabaseService } from '../../../services/database.service';

@Injectable({
    providedIn: 'root'
})
export class ValidateStartingKlm {
    constructor(private _databaseService: DatabaseService) { }

    validateKlm() {
        return (control: AbstractControl): Observable<{ [key: string]: any } | null> => {
            return this._databaseService.validateKlm(+control.value);
        }
    }
}