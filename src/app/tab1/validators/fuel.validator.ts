import { Injectable } from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';

@Injectable({
    providedIn: 'root'
})
export class ValidateFuel {
    fuelValidation() {
        return (control: AbstractControl): { [key: string]: any } | null => {
            const fuelCost = +control.get('fuelCost').value;
            const fuelCostPerLitre = +control.get('fuelCostPerLitre').value;
            const remainingKlm = +control.get('remainingKlm').value;
            if (fuelCost + fuelCostPerLitre + remainingKlm === 0) {
                return null;
            }
            return fuelCost > 0 && fuelCostPerLitre > 0 && remainingKlm > 0 ? null : { addAllFuelFields: true };
        };
    }
}