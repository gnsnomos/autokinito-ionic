import { Component, OnInit } from '@angular/core';
import { DatabaseService } from '../../services/database.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ValidateStartingKlm } from './validators/startingKlm.validator';
import { ValidateFuel } from './validators/fuel.validator';

@Component({
    selector: 'app-tab1',
    templateUrl: 'tab1.page.html',
    styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {

    public validations_form: FormGroup;

    constructor(public formBuilder: FormBuilder, private _databaseService: DatabaseService,
        private _customEmailValidator: ValidateStartingKlm,
        private _customFuelValidator: ValidateFuel) { }

    public ngOnInit(): void {

        this.validations_form = this.formBuilder.group({
            date: new FormControl('', Validators.compose([
                Validators.required
            ])),
            destination: new FormControl('', Validators.compose([
                Validators.maxLength(40),
                Validators.minLength(3),
                Validators.required
            ])),
            startingKlm: new FormControl('',
                Validators.compose([
                    Validators.maxLength(9),
                    Validators.minLength(1),
                    Validators.pattern('^[0-9]+$'),
                    Validators.required
                ]),
                this._customEmailValidator.validateKlm()
            ),
            fuel: this.formBuilder.group({
                fuelCost: new FormControl('', Validators.compose([
                    Validators.min(0),
                    Validators.pattern('([0-9]*[.])?[0-9]+')
                ])),
                fuelCostPerLitre: new FormControl('', Validators.compose([
                    Validators.min(0),
                    Validators.pattern('([0-9]*[.])?[0-9]+')
                ])),
                remainingKlm: new FormControl('', Validators.compose([
                    Validators.min(0),
                    Validators.pattern('^[0-9]+$')
                ]))
            }, { validator: this._customFuelValidator.fuelValidation() })
        });
    }

    public get startingKlm() {
        return this.validations_form.get('startingKlm');
    }

    public get fuel() {
        return this.validations_form.get('fuel');
    }

    public onDataSave(): void {
        this._databaseService.create_NewDestination(this.validations_form.value)
            .then(resp => {
                this.validations_form.reset();
            })
            .catch(error => {
                console.log(error);
            });
    }
}