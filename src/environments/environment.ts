// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyA9iRWczmO3XfyA3s8NU1vlZk_OOkzqehU",
    authDomain: "autokinito-app.firebaseapp.com",
    databaseURL: "https://autokinito-app.firebaseio.com",
    projectId: "autokinito-app",
    storageBucket: "autokinito-app.appspot.com",
    messagingSenderId: "863749021211",
    appId: "1:863749021211:web:fdf505ddf93c0ffd3a22be",
    measurementId: "G-1P06QDG2EF"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
