import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { map, take, debounceTime } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class DatabaseService {

    constructor(
        private _firestore: AngularFirestore
    ) { }


    create_NewDestination(record: any) {
        record.startingKlm = +record.startingKlm;
        record.fuel.fuelCost = +record.fuel.fuelCost;
        record.fuel.fuelCostPerLitre = +record.fuel.fuelCostPerLitre;
        record.fuel.remainingKlm = +record.fuel.remainingKlm;
        return this._firestore.collection('Destinations').add(record);
    }

    get_Destinations<Destination>(limit = 100) {
        return this._firestore.collection('Destinations', ref => ref
            .limit(limit)
            .orderBy('startingKlm', 'desc')
        ).snapshotChanges();
    }

    validateKlm(klmToValidate: number): Observable<{ [key: string]: any } | null> {
        return this._firestore.collection('Destinations', ref => ref.limit(1)
            .orderBy('startingKlm', 'desc')
        )
            .valueChanges().pipe(
                debounceTime(500),
                take(1),
                map(arr => {
                    return arr[0]['startingKlm'] >= klmToValidate ? { startingKlmLow: arr[0]['startingKlm'] } : null;
                }),
            )
    }

    update_Destination(recordId: string, record: any) {
        this._firestore.doc('Destinations/' + recordId).update(record);
    }

    delete_Destination(recordId: string) {
        this._firestore.doc('Destinations/' + recordId).delete();
    }
}